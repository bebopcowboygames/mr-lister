import discord
import spotipy
import config
import asyncio
import requests
from spotipy.oauth2 import SpotifyClientCredentials
from discord.ext import commands
from pickle import FALSE


spotify = spotipy.Spotify(client_credentials_manager=SpotifyClientCredentials())


intents = discord.Intents.default()
intents.message_content = True
bot = commands.Bot(command_prefix='.', intents=intents)

#Bot Events
@bot.event
async def on_ready():
    print('Online.')

@bot.event
async def on_message(message):
    await bot.process_commands(message)
    if message[0] == '.':
        return
    if message.author == bot.user:
        return
    pass


#bot commands
@bot.command()
async def facts(ctx, number):
        response = requests.get(f'http://numbersapi.com/{number}')
        await ctx.channel.send(response.text)

@bot.command()
async def boobs(ctx):
    await ctx.channel.send('The worst of all body parts.')

@bot.command()
async def user(ctx):
    user_info = spotify.user(config.self_user)
    await ctx.channel.send(user_info)

@bot.command()
async def playlists(ctx):
    list_of_playlists = spotify.user_playlists(config.self_user)
    await ctx.channel.send(list_of_playlists)

#now i want the above code snippet to be able to
#print the spotify URL for each playlist on Mr. Lister's account
#it currently does, but it's hidden in a lot of text
#not yet sure how to parse that



#bot setup
async def setup():
    print('Setting up...')

async def main():
    await setup()
    await bot.start(config.BOT_TOKEN)

#run the damn program
asyncio.run(main())


